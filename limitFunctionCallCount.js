// Should return a function that invokes `cb`.
// The returned function should only allow `cb` to be invoked `n` times.
// Returning null is acceptable if cb can't be returned

export function limitFunctionCallCount(cb, n) {
    let count = n; 
    
    return (() => {
        if(count){
            count--;
            return cb(count); 
        }
        else {
            return null; 
        }
    });
   
}