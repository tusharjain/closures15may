// Return an object that has two methods called `increment` and `decrement`.
// `increment` should increment a counter variable in closure scope and return it.
// `decrement` should decrement the counter variable and return it.
export const counterFactory = (() => {
    let count = 0; 
    const increment = function() {
        return ++count; 
    }
    const decrement = function() {
        return --count; 
    }
    return {increment, decrement};
})();