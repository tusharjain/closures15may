import {counterFactory} from '../counterFactory.js'; 

function test(func, expect) {
    let output = func(); 
    if(output === expect) {
        console.log('Test Passed\n');
    }
    else {
        console.log('Test Failed');
    }
}


test(counterFactory.increment, 1);
test(counterFactory.increment, 2); 
test(counterFactory.increment, 3);
test(counterFactory.decrement, 2);
test(counterFactory.decrement, 1);