import {cacheFunction} from '../cacheFunction.js';

function callback(arg) {
    return arg**2; 
}

let checkCacheFirst = cacheFunction(callback); 

function test(func, input, expect) {
    let output = func(input); 
    console.log(output); 

    if(output === expect) {
        console.log('Test Passed');
    }
    else {
        console.log('Test Failed');
    }
}

test(checkCacheFirst, 2, 4);
test(checkCacheFirst, 2, 4); 


