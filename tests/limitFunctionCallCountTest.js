import {limitFunctionCallCount} from '../limitFunctionCallCount.js'; 

function callback(c) {
    return c; 
}

const limitCalls = limitFunctionCallCount(callback, 4); 

function test(func, expect) {
    let output = func(); 
    if(output === expect) {
        console.log('Test Passed');
    }
    else {
        console.log('Test Failed');
    }
}

test(limitCalls, 3); 
test(limitCalls, 2); 
test(limitCalls, 1); 
test(limitCalls, 0); 
test(limitCalls, null); 
test(limitCalls, null); 